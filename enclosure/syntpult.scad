
potti=6.1;
touch=11;
led=3.1;
platte=4;
slidewidth=12;
slideheight=8;
slidebottom=6;
slidetol=1; // space around the slides
keywidth=200;
boxwidth=230;
setin=(boxwidth-keywidth)/2;
slidein=setin+10;
p2=platte+2*slidetol;

// generate your own control desk box ("pult gehaeuse").
laserpult(np1=4, //number of tooth on the  top skewed plate
             np2=4, //number of tooth on the lower (front) skewed plate
             a1=42, // angle of the top skewed plate
             a2=10, // angloe of the lower skewed plate
             x=boxwidth,  // width of the inner part 
             y=180, // widht from back to front (inner part)
             z1=125, // tooth on the back side
             z2=40,  // tooth on the front side
             nx=8,   // tooth on the bottom side (width)
             ny=7,   // tooth from back to front
             nz=5,   // tooth on the upper skewed plate
             nz2=2,  // tooth on the lower skewed plate
             kerf=0.08, // laser kerf
             thick=platte,   // tichknes of plate and length of tooth
             border=10, // the border around the side boxes (top, left, bottom and back and front)
             explode=12, // space between objects
             cut=false);  // cut=true => gerate svg outline. 

 // press F6 twice and then export svg
 



//sx=800;
//sy=1000;
//sz=1200;
//t=4;
//kerf=0.08;
tol=0.01;

//potticut();

module potticut(hasled=true,hastouch=true) {
    union() {
        translate([10,20,-platte]) cylinder(d=potti,$fn=16,h=4*platte);
        if (hasled) {
          translate([14,31+led/2,-platte]) cylinder(d=led,$fn=16,h=4*platte);
          translate([14,39-led/2,-platte]) cylinder(d=led,$fn=16,h=4*platte);
        }
        if (hastouch) {    
          translate([9,29.5,-platte]) cube([1,touch,4*platte]);
        }    
    }    
}    



module zackenplatte(x=200,y=200,thick=4,nx1=8,ny1=5,nx2=-1,ny2=-1,kerf=0,xlen=0) {
    
   nx2=  (nx2== -1) ? nx1 : nx2;
   ny2=  (ny2== -1) ? ny1 : ny2;
   k2=kerf/2;    
    
   union() { 
     cube([x+kerf,y+kerf,thick]);
     
     if (nx1 > 0 ) {
       z11=x/(1+2*nx1);   
       for(i=[0:nx1 -1 ]) translate([z11+2*z11*i,-thick-xlen+tol,0]) cube([  z11+kerf,    thick+xlen,thick]);
     } else if (nx1 < 0 ) {
       z11=x/(1-2*nx1);   
       for(i=[0: -nx1]) translate([2*z11*i,-thick-xlen+tol,0]) cube([  z11+kerf,    thick+xlen,thick]); 
     }
     
     if (nx2 > 0 ) {     
       z12=x/(1+2*nx2);    
       for(i=[0:nx2 -1 ]) translate([z12+2*z12*i,y-tol+kerf,0]) cube([z12+kerf,    thick+xlen,thick]);
     } else if (nx2 < 0 ) {
       z12=x/(1-2*nx2);    
       for(i=[0:-nx2]) translate([2*z12*i,y-tol+kerf,0]) cube([z12+kerf,    thick+xlen,thick]);

     }
     
     if (ny1 > 0 ) {    
        z21=y/(1+2*ny1);
        for(i=[0: ny1 - 1 ]) translate([-thick+tol-xlen,z21*(2*i+1)]) cube([thick+xlen,   z21+kerf ,thick]);
     } else if (ny1 < 0 ) {
        z21=y/(1-2*ny1);
        for(i=[0: -ny1 ]) translate([-thick+tol-xlen,z21*(2*i)]) cube([thick+xlen,   z21+kerf ,thick]);
     }
     
     if (ny2 > 0 ) {
       z22=y/(1+2*ny2);    
       for(i=[0: ny2 - 1 ]) translate([x-tol+kerf,z22*(2*i+1)]) cube([thick+xlen,   z22+kerf ,thick]); 
     } else if (ny2 < 0 ) {
       z22=y/(1-2*ny2);    
       for(i=[0: -ny2]) translate([x-tol+kerf,z22*(2*i)]) cube([thick+xlen,   z22+kerf ,thick]);   
     }     
   }      
}



//projection(cut=true) translate([0,0,-thick/2]) 
//for(i=[0:3]) {


 //zackenplatte(sx,sy,thick,nx1=3,ny1=4,kerf=kerf);
 //translate([0,0,sz]) zackenplatte(x=sx,y=sy,thick=thick,nx1=8,ny1=5,nx2=8,ny2=5); 
 //color("Blue") translate([-10,0,thick]) rotate([0,-90,0]) zackenplatte(x=sz,y=sy,thick=thick,nx1=3,ny1=5);


module sideplate(y=200,z1=500,z2=65,y0=0,z0=0,thick=4,kerf=0,border=20) {
    y0 = ( y0 == 0 ) ? y/2 : y0;
    z0 = ( z0 == 0 ) ? (z1+z2-border)/2 : z0;   
    
    color("Green")  translate([ -tol,0,0])  rotate([0,-90,0])  linear_extrude(height=thick,convexity=10)  polygon([[-border,-border],[-border,y+border],[z2+thick+border,y+border], [z0+border,y0], [z1+border+thick,border],[z1+border+thick,-border]],convexity=10);
    //cube([thick,y+2*border,z+2*border]); 
}   

module mycopytrans(vec) {
   children(0);
   //children([0:$children-1]);
   translate(vec) children(0); 
}    

module laserpult(x=50,y=50,z1=50,z2=25,a1=65,a2=20,nx=8,ny=11,nz=15,nz2=5,thick=4,kerf=0,explode=0,cut=false,side=true,xlen=0,np1=0,np2=0) {
    np1= (np1==0)  ? ny : np1;
    np2= (np2==0)  ? ny : np2;
    d2=thick*sin(a2);
    a1c=90-a1;
    h1=z1+thick;
    h2=h1+thick/sin(a1c);
    h3=z2+thick+thick*cos(a2);
    w=y+thick;
    y0=(h2-h3-w*tan(a2))/(tan(a1)-tan(a2));
    l2=(w-y0)/cos(a2)-thick;
    //y0=y/2;
    l1=y0/cos(a1)-thick*tan(a1);
    if (cut) { projection() {
        translate([0,0,-thick/2]) zackenplatte(x=x,y=y,nx1=nx,ny1=ny,kerf=kerf,thick=thick,xlen=xlen);
        //translate([0,0,z+kerf+thick+explode]) zackenplatte(x=x,y=y,nx1=nx,ny1=ny,kerf=kerf);
        color("Red") translate([0,y+thick+explode,-thick/2]) zackenplatte(x=x,y=z1,nx1=-nx,ny1=nz,kerf=kerf,thick=thick,xlen=xlen);
        color("Red") translate([0,-z2-thick-explode,-thick/2]) zackenplatte(x=x,y=z2,nx1=-nx,ny1=nz2,kerf=kerf,thick=thick,xlen=xlen);
        color("Lightblue") translate([0,-z2-thick-2*explode-l1 ,-thick/2]) zackenplatte(x=x,y=l1,nx1=nx,ny1=np1,nx2=0,kerf=kerf,thick=thick,xlen=xlen);
        color("Blue")  translate([0,-z2-thick-3*explode-l1-l2 ,-thick/2]) zackenplatte(x=x,y=l2,nx2=nx,ny1=np2,nx1=0,kerf=kerf,thick=thick,xlen=xlen);
        //color("Green") translate([-explode,0,thick]) rotate([0,-90,0]) zackenplatte(x=z,y=y,nx1=-nz,ny1=-ny,kerf=kerf);
        //color("Green") translate([x+thick+explode,0,thick]) rotate([0,-90,0]) zackenplatte(x=z,y=y,nx1=-nz,ny1=-ny,kerf=kerf);
      if (side) {
          
        mycopytrans( [0,-z1-thick-2*explode,0]) { 
           translate([-y-2*thick-explode,0,-thick/2]) rotate([0,-90,-90])
           difference() {  
              sideplate(y=y,z1=z1,z2=z2,thick=thick,border=border,y0=y0,z0=h2-y0*tan(a1));  
              laserpult(a1=a1,a2=a2,x=x,y=y,z1=z1,z2=z2,nx=nx,ny=ny,nz=nz,nz2=nz2,kerf=-kerf,thick=thick-kerf,  explode=0,side=false,xlen=3*thick,cut=false,np1=np1,np2=np2);
            }   
        } 
    }
   } 
  } else {
        translate([0,0,-explode]) zackenplatte(x=x,y=y,nx1=nx,ny1=ny,kerf=kerf,thick=thick,xlen=xlen);
        //translate([0,0,z+kerf+thick+explode]) zackenplatte(x=x,y=y,nx1=nx,ny1=ny,kerf=kerf);
        color("Red") translate([0,-thick-explode,z1+kerf+thick])  rotate([-90,0,0]) difference() {
            // backplane
            zackenplatte(x=x,y=z1,nx1=-nx,ny1=nz,kerf=kerf,thick=thick,xlen=xlen);
            translate([25,z1-13,-platte]) cylinder(d=9,$fn=16,h=4*platte);
            translate([39,z1-13,-platte]) cylinder(d=9,$fn=16,h=4*platte);
            translate([180,z1-13,-platte]) cylinder(d=6,$fn=16,h=4*platte);
            translate([150,z1-20,-platte]) cube([20,10,4*platte]);
            translate([200,z1-13,-platte]) cube([8,2.5,4*platte]);
        }   
        color("Red") translate([0,y+explode,z2+kerf+thick])  rotate([-90,0,0]) difference() {
            // front plate
            zackenplatte(x=x,y=z2,nx1=-nx,ny1=nz2,kerf=kerf,thick=thick,xlen=xlen);
            translate([slidein-slidetol,z2-slidebottom-slideheight+slidetol,-platte]) cube([slidewidth+2*slidetol,slideheight+tol+2*slidetol,4*platte]);
            translate([x-slidein-slidewidth,z2-slidebottom-slideheight+slidetol,-platte]) cube([slidewidth+2*slidetol,slideheight+tol+2*slidetol,4*platte]);
            translate([setin,z2-slidebottom-slideheight-platte-slidetol,-platte]) cube([keywidth,platte+2*slidetol,4*platte]);
           
        }    
        color("Lightblue") translate([0,0,z1+thick+explode*tan(a1)]) rotate([-a1,0,0]) difference() {
            zackenplatte(x=x,y=l1,nx1=nx,ny1=np1,nx2=0,kerf=kerf,thick=thick,xlen=xlen);
            // bohrungen sequencer
            for(i=[0:1]) for(j=[0:5]) {
                translate([j*20+20,i*40+5,-thick]) potticut();
            }
            translate([190,45,-thick]) potticut(hasled=false,hastouch=false);
            translate([165,25,-thick]) cube([26.6,19.3,,4*platte]);
            for(i=[0:1]) for(j=[0:1]) {
                translate([160+i*20,55+j*20,-thick]) cube([1,touch,4*platte]);
            }
            translate([200,10,-thick]) potticut(hasled=false,hastouch=false);    
        }   
        color("Blue")  translate([0,y-d2,z2+thick+explode+thick*sin(a2)])  rotate([-a2,0,0]) translate([0,-l2,0]) difference() {
            zackenplatte(x=x,y=l2,nx2=nx,ny1=np2,nx1=0,kerf=kerf,thick=thick,xlen=xlen);
            // bohrungen fuer synth
             for(i=[0:2]) for(j=[0:9]) {
                extra= (j > 7 ) ? 10 : ( (j > 3 ) ? 5: 0 ) ;
                translate([j*20+10+extra,i*30+5,-thick]) potticut(hasled=false,hastouch=false);
                 
            }
   
        }    
        //color("Green") translate([-explode,0,thick]) rotate([0,-90,0]) zackenplatte(x=z,y=y,nx1=-nz,ny1=-ny,kerf=kerf);
        //color("Green") translate([x+thick+explode,0,thick]) rotate([0,-90,0]) zackenplatte(x=z,y=y,nx1=-nz,ny1=-ny,kerf=kerf); 
      if (side) {
          
        mycopytrans( [x+thick+explode,0,0]) { 
           difference() {  
               sideplate(y=y,z1=z1,z2=z2,thick=thick,border=border,y0=y0,z0=h2-y0*tan(a1));  
              laserpult(a1=a1,a2=a2,x=x,y=y,z1=z1,z2=z2,nx=nx,ny=ny,nz=nz,nz2=nz2,kerf=-kerf,thick=thick-kerf,  explode=0,side=false,xlen=3*thick,cut=false,np1=np1,np2=np2);
            }   
        } 
    }
  }            
}    

blackkey=[0,1,1,1,2,2];

translate([-keywidth-20,0,-platte/2]) difference() {
     union() {
      cube([keywidth,100,platte]);
      translate([keywidth/2-10,100-tol,0]) cube([20,10,platte]);
     };
     for (i=[0:9]) {
       translate([5+i*20,65,-platte]) cube([touch,1,4*platte]);
     }    
     for (i=[0:5]) {
       bk=blackkey[i]+i;  
       translate([15+bk*20,30,-platte]) cube([touch,1,4*platte]);
     }    
         
}    



//laserpult(np1=3,np2=3,a1=50,a2=20,x=90,y=100,z1=120,z2=45,nx=4,ny=5,nz=5,nz2=2,kerf=0.08,thick=4,explode=15,cut=true);

//color("Pink") translate([0,150,20]) cube([240,1,1]);

//zackenplatte(x=200,y=200,thick=4,nx1=8,ny1=5,nx2=-1,ny2=-1,kerf=0,xlen=20) ;

