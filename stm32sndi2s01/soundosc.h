/* oscilator for synthisizer project
  
   copyright franz schaefer schaefe@mond.at
   under GNU GPL 3.0 or laster
   
   2018-aug-08 initial version

*/
   
#ifndef _SOUNDOSC_H_INCLUDED
#define _SOUNDOSC_H_INCLUDED


#define LFODIV 1000
#define ADSRDIV 32
#define ADSRFACT 20000

typedef struct envelop {
  int32_t a;
  int32_t d;
  int32_t s;
  int32_t r;
  int32_t amp;
  char    state; // a d s r or " ";
} ADSR_ENV ;

typedef enum {
  SND_SINE     =  0,
  SND_SQUARE   =  1,
  SND_TRIANGLE =  2,
  SND_SAW      =  3
  
} SND_TYPE;


class SoundOsc {

   public: 
     SoundOsc();
     void init(int32_t myrate);
     int32_t nextval(void);
     void set_adsr(int32_t seta, int32_t setd, int32_t sets, int32_t setr);
     void init_adsr(void) ;
     int32_t next_adsr(void);
     void setstate(int mystate) ;
     void setlfofreq(int32_t f) ;  // in miliHz
     void setfreq(int32_t f) ; // in Hz
     void setwavelfo(SND_TYPE t) ;
     void setwave(SND_TYPE t) ;
     void setvolume(int32_t v);
     void set_noisefm(int32_t  nfm);
     void set_noiseam(int32_t  nam);
     void set_lfo_am(int32_t  lam);
     void set_lfo_fm(int32_t  lfm);
          
        
//   private:
     uint32_t cnt;
     uint32_t rsum;
     uint32_t delta;
     uint32_t freq;
     uint32_t rsumlfo;
     uint32_t deltalfo;
     uint32_t freqlfo;
     SND_TYPE type;
     SND_TYPE typelfo;
     
     int32_t noiseam;
     int32_t noisefm;
     int32_t lfofm;
     int32_t lfoam;
     ADSR_ENV adsr;
     uint32_t rate;
     int32_t lastlfo;
     int32_t volume;
          
     int  state; // 0 off, 1 on;
     int  statecnt; // count value of last state change 
     int  active; // 0 not running anymore
     
   protected:

   private: 
   	
	
};



#endif	
