/* oscilator for synthisizer project
  
   copyright franz schaefer schaefe@mond.at
   under GNU GPL 3.0 or laster
   
   2018-aug-08 initial version

*/

//#include <inttypes.h>   
#include <Arduino.h>   
#include "soundosc.h"

extern "C" {
  extern int16_t sintab[] ;
//  extern uint16_t nextlfsr(void);
}
// blah
//static uint16_t lfsr ;

static uint16_t lfsr;

uint16_t getlfsr() {
   uint16_t xbit;
   if (lfsr == 0 ) lfsr=0xACE1u;
   xbit  = ((lfsr >> 0) ^ (lfsr >> 2) ^ (lfsr >> 3) ^ (lfsr >> 5) ) & 1;
   lfsr =  (lfsr >> 1) | (xbit << 15);
   return lfsr;
}

int32_t index2wave(SND_TYPE t, int ndx) {
  int32_t oscval;
  switch(t) {
    int ramp;
    case SND_SINE: 
             oscval=sintab[ndx];
            break;
    case SND_SQUARE:    
             oscval= ( (ndx & 0x80 ) == 0 ) ? -10000 : 10000 ;
            break;
    case SND_TRIANGLE:
             ramp=ndx & 0x3f ;
             if ( (ndx & 0x40  ) != 0 ) ramp = 0x40 - ramp ;       
             if ( (ndx  & 0x80 ) != 0 ) ramp = 0-ramp ;
             oscval=ramp*156;
            break;
    case SND_SAW:
             ramp=ndx & 0x7f ;
             oscval=(ramp-64)*156;
            break;
    default:
             oscval=0;        
   }         
   return oscval;
}


void SoundOsc::init(int32_t myrate) {
  cnt=0;
  rsum=0;
  delta=0;
  freq=0;
  rsumlfo=0;
  deltalfo=0;
  freqlfo=0;
  type=SND_SINE;
  typelfo=SND_SINE;
  noiseam=0;
  noisefm=0;
  lfofm=0;
  lfoam=0;
  set_adsr(0,0,10000,0);
  init_adsr();
  active=0;
  rate=myrate ;
  lastlfo=0;
  volume=10000;
};


void SoundOsc::setfreq(int32_t f) {
  freq=f;
  //tonea=(int)(440L*10000L*256/(rate/2)+1L)/2;
  delta=2560000L*freq/rate;   
}

void SoundOsc::setwave(SND_TYPE t) {
  type=t;
}

void SoundOsc::setwavelfo(SND_TYPE t) {
  typelfo=t;
}

void SoundOsc::setlfofreq(int32_t f) {  // in miliHz
  freqlfo=f;
  //tonea=(int)(440L*10000L*256/(rate/2)+1L)/2;
  deltalfo=25600L*freqlfo/rate*LFODIV/100;   
}

SoundOsc::SoundOsc() {
}


void SoundOsc::set_adsr(int32_t seta, int32_t setd, int32_t sets, int32_t setr) {
  adsr.a=seta;
  adsr.d=setd;
  adsr.s=sets;
  adsr.r=setr;
}

void SoundOsc::init_adsr() {
  adsr.amp=0;
  adsr.state=' ';
}

void SoundOsc::setvolume(int32_t v) {
   volume=v;
}

void SoundOsc::set_noisefm(int32_t nfm) {
   noisefm=nfm;
}

void SoundOsc::set_noiseam(int32_t nam) {
   noiseam=nam;
}


void SoundOsc::set_lfo_am(int32_t lam) {
   lfoam=lam;
}


void SoundOsc::set_lfo_fm(int32_t fam) {
   lfofm=fam;
}




int32_t SoundOsc::next_adsr() {

  switch(adsr.state) {
     case 'a': 
         if (adsr.a < 14 ) {
           adsr.amp=10000; 
         } else {
           adsr.amp += (10000 - adsr.amp) * adsr.a / ADSRFACT ;
         } 
         if (adsr.amp >= 9900 ) {
           adsr.amp=10000;
           adsr.state='d';
         }
         if (state == 0 ) {
           adsr.state='r';
         } 
       break;
     case 'd':
         if (adsr.d > 9900 ) {
           adsr.amp=adsr.s; 
         } else {
           adsr.amp -= (adsr.amp - adsr.s ) * adsr.d / ADSRFACT ; 
         } 
         if (adsr.amp <=  (adsr.s + 100)   ) {
           adsr.amp=adsr.s;
           adsr.state='s';
         } 
         if (state == 0 ) {
           adsr.state='r';
         } 
        
       break;
                 
     case 's':
        adsr.amp=adsr.s;
        if (state == 0 ) {
          adsr.state='r';
          if (adsr.r > 9900  )  {
            adsr.amp=0;
          }
        }
       break;
                 
     case 'r':
         if (adsr.r == 0 ) {
           adsr.amp=0;
         } else {
           adsr.amp -= adsr.amp  * adsr.r / ADSRFACT ;  
         } 
         if (adsr.amp >  9900   ) {
           adsr.amp=0;
           adsr.state=' ';
           active=0;
         } 
        
       break;
                 
     case ' ':
       if (active && state) {
         adsr.state='a';
         adsr.amp=0;
       }
       break;

     default:
       adsr.state=' ';
       adsr.amp=0;
       break;
     
  }
  return adsr.amp;
}


void SoundOsc::setstate(int mystate) {
  if (state == 0 and mystate != 0 ) {
    adsr.state=' ';
    adsr.amp=0;
    rsum=0;
    rsumlfo=0;
  }  
  state=mystate;
  statecnt=cnt;
  if (state) active=1;
}

int32_t SoundOsc::nextval() { 
  int32_t val=0;
  //val=getlfsr();
  //return val;
  
  if (active) {
    if (cnt % LFODIV == 0) {
      rsumlfo += deltalfo ;
      if (rsumlfo >= 2560000 ) rsumlfo -= 2560000 ;
      int ndxlfo= ( rsumlfo/10000 ) % 256 ; 
      lastlfo=index2wave(typelfo,ndxlfo);
    }
    if (cnt % ADSRDIV == 0) {
      next_adsr(); 
    }
    int32_t realdelta=delta;
    if ( lfofm > 40 ) {
      realdelta += (( ((int32_t)(realdelta)) * lastlfo) / 10000 )  * lfofm / 5000 ;
    }
    
    if ( noisefm > 40 ) {
      realdelta += ( ((int32_t)realdelta) * (32768-((uint16_t)getlfsr()) ) / 256000 ) * noisefm / 1280 ; 
    }
    rsum += realdelta; 
    
    if (rsum >= 2560000 ) rsum -= 2560000 ;
    int ndx= ((int)( rsum/10000 )) % 256 ; 
    val=index2wave(type,ndx);
    val=(val*adsr.amp)/10000;
    if ( lfoam > 13 ) {
      val +=  ((val* lastlfo) / 10000) * lfoam / 2500 ;  
    }
    if (noiseam > 13 ) {
      val +=   (val * (32768-((uint16_t)getlfsr())) / 10000) * noiseam / 32768  ;
    }
    val=(val*volume)/10000; 
  } 
  cnt++;  
  return val;
}

