// Copyright: franz schaefer schaefe@mond.at
// GNU GPL 3.0 or later
// 

#include <Wire.h>
#include <SoftWire.h>

#include <Font32.h>
#include <Adafruit_GFX_AS.h>
#include <Font7s.h>
#include <Font16.h>
#include <Load_fonts.h>
#include <Font64.h>


#include <Adafruit_SSD1306_STM32.h>

#include "soundosc.h"


/*
  Reading a serial ASCII-encoded string.
 
 This sketch demonsts the Serial parseInt() function.
 It looks for an ASCII string of comma-separated values.
 It parses them into ints, and uses those to fade an RGB LED.
 
 Circuit: Common-anode RGB LED wired like so:
 * Red cathode: digital pin 3
 * Green cathode: digital pin 5
 * blue cathode: digital pin 6
 * anode: +5V
 
 created 13 Apr 2012
 by Tom Igoe
 
 This example code is in the public domain.

SPI2 PB13 clk, PB15 dout, PB13 #include <series/stm32.h>  CS
D/C C7
 
 */

/*
#undef MCU_STM32F103C8
#define MCU_STM32F103T8
#define STM32_F1_LINE                3

//#define MCU_STM32F103RC
//#include <stm32.h>
#include "/home/mond/stm32/Arduino_STM32/STM32F1/libraries/SPI/src/SPI.h"
*/
//
#include <SPI.h>
#include "I2S.h"
// #include "nokia5110.h"
//#include "font5x7.h"
#include "sounds.h"

#define SPI1_NSS_PIN PA4    //SPI_1 Chip Select pin is PA4. You can change it to the STM32 pin you want.
#define SPI2_NSS_PIN PB12

#define WS   PB12
#define BCK  PB13
#define DATA PB15


Adafruit_SSD1306 oled(PC14);
//TwoWire Wire2(2);
#define WireX Wire

uint8_t mary[]={2, 2, 1, 2, 0, 2, 1, 2, 2, 2, 2, 2, 
        2, 4, 1, 2, 1, 2, 1, 4, 2, 2, 4, 2,
        4, 4, 2, 2, 1, 2, 0, 2, 1, 2, 2, 2,
        2, 2, 2, 2, 2, 2, 1, 2, 1, 2, 2, 2,
        1, 2, 0, 4, 0, 0};
#define MLEN (sizeof(mary)/2)        
#define ECHOLEN 10240        
int16_t echobuf[ECHOLEN];
int eindex;

volatile int dummy;

//const int rate=46875 ; //  45000;
const int rate=41826 ; //  45000;
//const int rate=22000; 
const int tonea=(int)(440L*10000L*256/(rate/2)+1L)/2;
//const int tonea=26931;
const int ofact=10000*tonea/oct1[12];
int sec;
int beats;
int oldbeat;

void debug_text(char *ptxt) {
  oled.print(ptxt);
  
};

void debug_digit(int digit) {
  oled.print(digit);
};

void debug_bin(uint16_t digit) {
  oled.setCursor(0,0);
  for(int i=0; i < 16 ; i++) {
    int val= 0x1 & digit  ;
    oled.print(val);
    digit = digit >> 1 ; 
  }
  oled.print(".\n");  
};


void debug_update(void) {
  oled.display();
};

int maxwait; 
//uint8_t nokia[504];

// pins for the LEDs:digitalWrite(led, HIGH);
const int led = PB11;  // hier wird die pin nummer von der led definiert
const int testpin=PC14;
//const int greenPin = 5;
//const int bluePin = 6;
int32_t loopcnt;
int dispupdate; 
int dispop;

//SPIClass SPI_nokia(1);
I2SClass I2S(SPI2);

uint16_t start_state = 0xACE1u;  /* Any nonzero start state will work. */
uint16_t lfsr = start_state;
uint16_t bit;
    
uint16_t touch_state;

//#define TOUCH_ADDRESS (0xAF>>1)
#define TOUCH_ADDRESS 0x57   

#define ANAIN PA0   /* lila */
#define MUXAEN0 PA1 /* white */
#define MUXA0 PA2   /* yellow */ 
#define MUXA1 PA3   /* brown */
#define MUXA2 PA4   /* green */ 
#define MUXA3 PA5   /* blue */
#define MUXAEN1 PB0 /* braun */ 
#define MUXAEN2 PB1 /* grau */ 


typedef enum { 
  POT_VOL      = 0, 
  POT_TYPE     = 1,
  POT_NOISE_FM = 2,
  POT_NOISE_AM = 3,

  POT_EXTRA    = 4,
  
  POT_ADSR_A   = 5,
  POT_ADSR_D   = 6,
  POT_ADSR_S   = 7,
  POT_ADSR_R   = 8,

  POT_LFO_TYPE = 9,
  POT_LFO_FM   = 10,
  POT_LFO_AM   = 11,
  POT_LFO_F    = 12,

  POT_PITCH    = 13,
  POT_ECHO_DELAY = 14,
  POT_ECHO       = 15,
} POTTINUM ;

uint16_t echodel;
int32_t  echoamp;

//normalize potvalues from 12 bit to 0-10000; 
#define PNORM(X) ((X*625) >> 8)

#define PNORM4(X) ((SND_TYPE)(X >> 10) )
#define PNORMEXP(X) ( (exptab[X] + 2) >> 2  )

void pottisetosc( uint16_t *ppot, SoundOsc *posc) {
  posc->setvolume(PNORM(ppot[POT_VOL]));
  posc->set_adsr(PNORMEXP(ppot[POT_ADSR_A]),PNORMEXP(ppot[POT_ADSR_D]),PNORMEXP(ppot[POT_ADSR_S]),PNORMEXP(ppot[POT_ADSR_R]));
  posc->setwave(PNORM4(ppot[POT_TYPE]));
  posc->setwavelfo(PNORM4(ppot[POT_LFO_TYPE]));
  //posc->setlfofreq(2*PNORM(ppot[POT_LFO_F]));
  posc->setlfofreq(4*PNORMEXP(ppot[POT_LFO_F]));
  posc->set_lfo_am(PNORM(ppot[POT_LFO_AM]));
  posc->set_lfo_fm(PNORM(ppot[POT_LFO_FM]));
  posc->set_noisefm(PNORM(ppot[POT_NOISE_FM]));
  posc->set_noiseam(PNORM(ppot[POT_NOISE_AM]));
}

void pottiglobal( uint16_t *ppot) {
  echoamp=PNORM(ppot[POT_ECHO]);
  echodel=PNORM(ppot[POT_ECHO_DELAY]);
}


int rsum;
int delta;
int notbusy;

uint16_t potti[48];
uint16_t nextpotti;

void selnextpotti(void) {
  nextpotti++;
  nextpotti = nextpotti % 48 ; 
  selpotti(nextpotti);
}

void selpotti(int pot) {
  pot = pot % 48 ; 
  uint16_t val=pot % 16 ; 
  digitalWrite(MUXAEN0,HIGH);
  digitalWrite(MUXAEN1,HIGH);
  digitalWrite(MUXAEN2,HIGH);

  digitalWrite(MUXA0, (val & 0x1 ));  val=val >> 1 ; 
  digitalWrite(MUXA1, (val & 0x1 ));  val=val >> 1 ; 
  digitalWrite(MUXA2, (val & 0x1 ));  val=val >> 1 ; 
  digitalWrite(MUXA3, (val & 0x1 )); 
  if (pot < 16 ) {
    digitalWrite(MUXAEN0,LOW);
  } else if ( pot < 32) {
    digitalWrite(MUXAEN1,LOW);
  } else {
    digitalWrite(MUXAEN2,LOW);
  }

}

void showpotti(void) {
   for(int j=0; j < 3 ; j++) {
     oled.fillRect(70+j*20,10, 16, 32, 0);
     for(int i=0; i<16 ; i++) {
        int v=potti[i+j*16] / 256 ;
        oled.fillRect(70+j*20,10+2*i, v, 1, 1);
     }
   }
}

uint16_t nextlfsr() {
   uint16_t xbit;
   xbit  = ((lfsr >> 0) ^ (lfsr >> 2) ^ (lfsr >> 3) ^ (lfsr >> 5) ) & 1;
   lfsr =  (lfsr >> 1) | (xbit << 15);
   return lfsr;
}

#define NOSC 4

SoundOsc osc[NOSC];


void setup() {
  int i;

  pinMode(ANAIN,INPUT);
  pinMode(MUXAEN0,OUTPUT);
  pinMode(MUXAEN1,OUTPUT);
  pinMode(MUXAEN2,OUTPUT);
  digitalWrite(MUXAEN0, HIGH);
  digitalWrite(MUXAEN1, HIGH);
  digitalWrite(MUXAEN2, HIGH);

  pinMode(led, OUTPUT); 
  for(i=0; i<5 ; i++) {
     digitalWrite(led, LOW);
     delay(200);
     digitalWrite(led, HIGH);
     delay(200);
  }
  Wire.begin();
  Wire.setClock(400000);
  notbusy=0;  
  pinMode(MUXA0,OUTPUT);
  pinMode(MUXA1,OUTPUT);
  pinMode(MUXA2,OUTPUT);
  pinMode(MUXA3,OUTPUT);
  nextpotti=0; 
   
  oled.begin( SSD1306_SWITCHCAPVCC, 0x3c);
  oled.fillScreen(0);
  oled.drawRect(0,0, 128, 64, 1);
  oled.setCursor(0,0);
  oled.setTextColor(1,0);
  oled.setTextSize(1);
  oled.setTextWrap(true);
  
  //oled.drawRect(2,2, 6, 6, 1);
  //oled.display();
  delay(1000);
  
  //Serial.print("hello world\n");
  loopcnt=0;
  
  if (!I2S.begin( I2S_PHILIPS_MODE , rate, 16,false)) {  // I2S_LEFT_JUSTIFIED_MODE
    debug_text("init of i2s failed\n");
  } else {
    debug_text("starting\n");
  }
  int realrate=I2S.getRate();
  debug_digit(realrate);
  debug_text(" = rate\n");
  debug_update();
  for(i=0;i<20;i++) {
      digitalWrite(led, HIGH);
      delay(50);
      digitalWrite(led, LOW);
      delay(50);
  }
  delay(1000);
  
  rsum=0;
  delta=1;
  sec=0;
  beats=0;
  for(int i=0; i < ECHOLEN ; i++) {
    echobuf[i]=0;
  }
  eindex=0;
  dummy=1;
  lfsr = start_state; 
  WireX.begin();
  selnextpotti();
  dispupdate=0;
  dispop=0;
  for(i=0; i < NOSC ; i++) {
    osc[i].init(rate);
  }
  osc[0].setfreq(200);
  osc[0].setstate(1);
  
  osc[0].setwave(SND_TRIANGLE);
  osc[0].set_adsr(30,300,0000,0);
  osc[0].set_adsr(0,0,10000,0);
  osc[0].setwave(SND_SINE);
  oldbeat=-1;
}

void loop() {
  int i;
  uint8_t b1,b2;
  int nread=0;
  //int oldbeat=-1;
  for(;;loopcnt++) {
    loopcnt++;
    beats=  loopcnt /(  rate / 4 );
    int nwait=0;
    int mcnt=sizeof(mary)/2;
    int idx = beats / 4 ;  
    idx= idx % MLEN; 
    int mlen=mary[idx*2+1];
    if (beats != oldbeat) { 
      if (beats % 4  == 0 ) {
        // osc[0].setfreq(1000 * potti[idx]/512 );
        osc[0].setfreq(1000 * oct1[mary[idx*2]]/10000 );
        //osc[0].setfreq(400);
        osc[0].setstate(1);
      } else if (  beats % 4  == mlen ) {
        osc[0].setstate(0);
      }
      oldbeat=beats;
    }  
    
    //int32_t val=nextlfsr(); //osc[0].nextval();
    int32_t val=osc[0].nextval();
    if (echoamp > 0  && echodel > 0 ) {
      val= (val*(10000-echoamp)+(echoamp*echobuf[eindex]))/10000;
    }
    //val= nextlfsr() ;   
    echobuf[eindex]=val;
    eindex++ ;
    // eindex = eindex % ECHOLEN ; 
    if (echodel > 0 ) {
      eindex = eindex % echodel ; 
    } else {
      eindex = 0; 
    }
    dispupdate++; 
    while (! I2S.availableForWrite() ) {
      if (dispupdate > 10000 ) {
        dispupdate=0;
        
        dispop++ ;
        dispop %= 3 ;
        //dispop=3;
        switch(dispop) {
          case 0: 
             //digitalWrite(led, HIGH);
  
            oled.display();
            break;
          case 1:
              //digitalWrite(led, LOW);
              WireX.requestFrom(TOUCH_ADDRESS, 2);
              maxwait=10;
              while(WireX.available() < 1 and maxwait > 0 ) {
                delay(1);
                maxwait--;
              }
              b1=WireX.read();
              b2=WireX.read();
              
              touch_state = ( b1 << 8 ) | b2; 
              debug_bin(touch_state);
              debug_text(".\n rsum: ");
              debug_digit(osc[0].delta);
              
              debug_text(".\n nread=");
              debug_digit(nread);
              debug_text(".\n potti12=");
              debug_digit(potti[12]);
              debug_text(".\n potti0=");
              debug_digit(potti[0]);
              debug_text(".\n");
              showpotti();
              //osc[0].setvolume(loopcnt % 10000 );     
            break;
          case 2:
            pottisetosc(&potti[0],&osc[0]);
            pottiglobal(&potti[0]);
 
             break;     
        }
      } else {  
        //delay(1);
        delayMicroseconds(50);
        potti[nextpotti]=  (potti[nextpotti] * 4 + analogRead(ANAIN)) / 5 ;
        nread++; 
        selnextpotti();
        nwait++;
      }  
    } 
    digitalWrite(led, LOW); 
    I2S.write(val);
    I2S.write(val);
    digitalWrite(led, HIGH); 
    
  } 
}
